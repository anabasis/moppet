// moppet, an alternative frontend for PBS Kids
// Copyright (C) 2022 anabasis
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strings"
	"time"
)

func Live(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "max-age=604800")
	w.Header().Set("Content-Type", "text/html")

	t, err := template.New("live.gohtml").Funcs(template.FuncMap{
		"ProxyImage": ProxyImage,
	}).ParseFS(content, "tmpl/live.gohtml")
	if err != nil {
		log.Println(err)
	}

	err = t.Execute(w, HomeData(r.Context()).Collection.Livestream.Content[0].URL)
	if err != nil {
		log.Println(err)
	}
}

func Watch(w http.ResponseWriter, r *http.Request) {
	split := strings.Split(r.URL.Path, "/")

	if len(split) == 3 && split[len(split)-1] == "" {
		http.Redirect(w, r, "/", http.StatusMovedPermanently)

		return
	}

	if len(split) == 3 || len(split) == 4 || (len(split) == 5 && split[len(split)-1] == "") {
		split[1] = "shows"
		http.Redirect(w, r, strings.Join(split[:3], "/"), http.StatusMovedPermanently)

		return
	}

	if len(split) != 5 {
		Error(w, r)

		return
	}

	data := ShowsData(split[2], r.Context())

	var category []video

	switch split[3] {
	case "episodes":
		category = data.Collection.Episodes.Content
	case "clips":
		category = data.Collection.Clips.Content
	case "promoted_content":
		category = data.Collection.PromotedContent.Content
	}

	var video video

	for _, item := range category {
		if item.ID == split[4] {
			video = item

			break
		}
	}

	if video.Title != "" {
		video.Program.Slug = data.Info.Slug

		w.Header().Set("Cache-Control", "max-age=86400")
		w.Header().Set("Content-Type", "text/html")

		t, err := template.New("watch.gohtml").Funcs(template.FuncMap{
			"FormatDate":  FormatDate,
			"CheckMP4":    CheckMP4,
			"GetRedirect": GetRedirect,
			"GetCaptions": GetCaptions,
			"ProxyImage":  ProxyImage,
		}).ParseFS(content, "tmpl/watch.gohtml")
		if err != nil {
			log.Fatalln(err)
		}

		err = t.Execute(w, video)
		if err != nil {
			log.Println(err)
		}
	} else {
		Error(w, r)
	}
}

func FormatDate(air time.Time, upload time.Time, expire time.Time) (out string) {
	const USDate = "January 2, 2006"
	if air.Equal(upload) {
		out = fmt.Sprintf("Aired and added on %v.", air.Format(USDate))
		if expire.Second() != 0 {
			out += fmt.Sprintf(" Expires on %v.", expire.Format(USDate))
		}
	} else {
		out = fmt.Sprintf("Aired on %v. Added on %v.", air.Format(USDate), upload.Format(USDate))
		if expire.Second() != 0 {
			out += fmt.Sprintf(" Expires on %v.", expire.Format(USDate))
		}
	}

	return
}

func CheckMP4(mp4 string, hls string) template.HTML {
	if mp4 == hls {
		return ""
	}

	return `
				<source src="` + template.HTML(GetRedirect(mp4)) + `" type="video/mp4">`
}

func GetRedirect(redirect string) string {
	if !inUS || redirect == "" {
		return redirect
	}

	data, found := c.Get("url/" + redirect)
	if found {
		return data.(string)
	}

	client := &http.Client{}

	req, err := http.NewRequest(http.MethodHead, redirect, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", userAgent)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	url := resp.Request.URL.String()

	c.Set("url/"+redirect, url, 7*24*time.Hour)

	return url
}

func GetCaptions(captionList closedCaptions) string {
	for _, x := range captionList {
		if x.Format == "WebVTT" {
			return x.URL
		}
	}
	return ""
}
