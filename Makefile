NAME=moppet
VERSION=v0.1.0

all: build

build:
	go build -trimpath -ldflags="-w -s" -o ${NAME} .

build_all:
	mkdir -p ./builds
	rm -f ./builds/*
	GOOS=linux GOARCH=amd64 go build -trimpath -ldflags="-w -s" -o ./builds/${NAME}-linux_amd64 .
	GOOS=windows GOARCH=amd64 go build -trimpath -ldflags="-w -s" -o ./builds/${NAME}-windows_amd64 .
	tar czf ./builds/${NAME}-linux_amd64-${VERSION}.tar.gz --owner=0 --group=0 ./builds/${NAME}-linux_amd64
	zip -j ./builds/${NAME}-windows_amd64-${VERSION}.zip ./builds/${NAME}-windows_amd64
	find . -type f -executable -exec rm '{}' \;

run:
	go build -trimpath -ldflags="-w -s" -o ${NAME} .
	./${NAME}

install:
	go install -trimpath -ldflags="-w -s"

upgrade:
	@echo "upgrading Go dependencies"
	go get -u ./...
	@echo "upgrading JavaScript dependencies"
	yarn upgrade --use-yarnrc "${XDG_CONFIG_HOME}/yarn/config"
	cp node_modules/video.js/dist/video-js.min.css node_modules/video.js/dist/video.min.js node_modules/videojs-contrib-quality-levels/dist/videojs-contrib-quality-levels.min.js node_modules/videojs-hls-quality-selector/dist/videojs-hls-quality-selector.min.js node_modules/videojs-hotkeys/videojs.hotkeys.min.js static/assets/js/lib/

clean:
	go clean
	rm ./builds/${NAME}*
	rm ${NAME}

lint:
	golangci-lint run --enable-all

vet:
	go vet

fmt:
	gofmt -s -w .
