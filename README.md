# moppet
An alternative frontend for PBS Kids

* watch all of your favorite PBS Kids shows with no need to execute non-free JavaScript
* does not include any ads or trackers; the PBS Kids website includes analytics and trackers from CrazyEgg, Google (Google Analytics and Google Tag Manager), New Relic, and Comscore (ScorecardResearch)
	- video content is still served from Amazon CDNs
* fully functional without JavaScript enabled
* free (as in freedom) software released under a copyleft license (AGPLv3+)

## Instructions
Download the latest release or install using `go install`.
It will default to port 80 (this can be change by passing the port number with the `-port` flag).

### Deployment
It is recommended to deploy moppet behind a reverse proxy for caching.

It is highly recommended for the server to be deployed in a server located in the US (or make requests with an US IP) because PBS redirect service does not allow requests due to CORS; clients will be forced to use the website with JavaScript disabled (Video.js won't work).
To check if your IP is considered to be an IP located in the US, check by running `curl https://localization.services.pbs.org/localize/auto/`.

## License
This project is licensed under the GNU Affero General Public License, version 3 or later. See `LICENSE` for terms and view [a copy of license](https://www.gnu.org/licenses/agpl-3.0.en.html).

## Acknowledgements
* Go
	- [Colorblind](https://github.com/afonsolopez/colorblind) (GPL-3.0)
	- [diskcache](https://github.com/gregjones/httpcache/tree/master/diskcache) (MIT)
	- [httpcompression](https://github.com/CAFxX/httpcompression) (Apache-2.0)
	- [imageproxy](https://github.com/willnorris/imageproxy) (Apache-2.0)
	- [LruCache](https://github.com/die-net/lrucache) (Apache-2.0)
	- [TwoTier](https://github.com/die-net/lrucache/tree/main/twotier) (Apache-2.0)
* JavaScript
	- [Video.js](https://github.com/videojs/video.js) (Apache-2.0)
	- [videojs-hotkeys](https://github.com/ctd1500/videojs-hotkeys) (Apache-2.0)
	- [videojs-contrib-quality-levels](https://github.com/videojs/videojs-contrib-quality-levels) (Apache-2.0)
	- [videojs-hls-quality-selector](https://github.com/chrisboustead/videojs-hls-quality-selector) (MIT)
	- [Videojs Themes](https://github.com/videojs/themes) (MIT)
* Fonts
	- [League Spartan](https://github.com/theleagueof/league-spartan) (OFL-1.1)
	- [Lexend Deca](https://github.com/googlefonts/lexend) (OFL-1.1)
	- Icons from [Material Design Icons](https://github.com/google/material-design-icons) (Apache-2.0)

## Screenshots
![Screenshot of home page](screenshots/home.png)
![Screenshot of show homepage](screenshots/show.png)
![Screenshot of player page](screenshots/player.png)
