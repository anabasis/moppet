// moppet, an alternative frontend for PBS Kids
// Copyright (C) 2022 anabasis
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"html/template"
	"io/fs"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func Error(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(404)
	w.Header().Set("Cache-Control", "max-age=604800")
	w.Header().Set("Content-Type", "text/html")

	t, err := template.ParseFS(content, "tmpl/error.gohtml")
	if err != nil {
		log.Println(err)
	}

	err = t.Execute(w, nil)
	if err != nil {
		log.Println(err)
	}
}

func Static(w http.ResponseWriter, r *http.Request) {
	if strings.HasPrefix(r.URL.Path, "/assets/fonts/") || strings.HasPrefix(r.URL.Path, "/assets/img/") {
		w.Header().Set("Cache-Control", "max-age=31536000")
	} else {
		w.Header().Set("Cache-Control", "max-age=1209600")
	}

	fsys, err := fs.Sub(content, "static")
	if err != nil {
		log.Println(fsys)
	}
	// TODO: check if file exists else send error
	http.FileServer(http.FS(fsys)).ServeHTTP(w, r)
}

func Redirect(w http.ResponseWriter, r *http.Request) {
	host := r.Host
	if strings.Split(*secureAddr, ":")[0] == "" {
		host = strings.Split(r.Host, ":")[0] + ":" + strings.Split(*secureAddr, ":")[1]
	} else if strings.Split(r.Host, ":")[0] != strings.Split(*secureAddr, ":")[0] {
		host = *secureAddr
	}

	target := url.URL{
		Scheme:   "https",
		Host:     host,
		Path:     r.URL.Path,
		RawQuery: r.URL.RawQuery,
	}

	http.Redirect(w, r, target.String(), http.StatusPermanentRedirect)
}

func Proxy(w http.ResponseWriter, r *http.Request) {
	// TODO: override proxy's headers (Cache-Control, X-XSS-Protection)
	// w.Header().Set("Cache-Control", "max-age=31536000")
	r.URL.Path = strings.Replace(r.URL.Path, "/proxy/", "/", 1)

	if r.URL.Path != "" {
		proxy.ServeHTTP(w, r)
	} else {
		Error(w, r)
	}
}
