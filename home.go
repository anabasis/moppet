// moppet, an alternative frontend for PBS Kids
// Copyright (C) 2022 anabasis
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"html/template"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/afonsolopez/colorblind"
	"willnorris.com/go/imageproxy"
)

type HomeStruct struct {
	Collection struct {
		Livestream Livestream `json:"kids-livestream"`
		Spotlight  Show       `json:"kids-show-spotlight"`
		Programs1  Show       `json:"kids-programs-tier-1"`
		Programs2  Show       `json:"kids-programs-tier-2"`
		Programs3  Show       `json:"kids-programs-tier-3"`
	} `json:"collections"`
}

type Show struct {
	Content []struct {
		Title      string `json:"title"`
		Ages       string `json:"ages"`
		Background string `json:"background_color"`
		Slug       string `json:"slug"`
		Images     struct {
			Logo       string `json:"logo"`
			Background string `json:"background"`
		} `json:"images"`
	} `json:"content"`
}

type Livestream struct {
	Content []struct {
		URL string `json:"URI"`
	} `json:"content"`
}

func Home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "" && r.URL.Path != "/" {
		Error(w, r)

		return
	}

	t, err := template.New("home.gohtml").Funcs(template.FuncMap{
		"Foreground": Foreground,
		"FormatAges": FormatAges,
		"ProxyImage": ProxyImage,
	}).ParseFS(content, "tmpl/home.gohtml")
	if err != nil {
		log.Println(err)
	}

	w.Header().Set("Cache-Control", "max-age=604800")
	w.Header().Set("Content-Type", "text/html")

	err = t.Execute(w, HomeData(r.Context()))
	if err != nil {
		log.Println(err)
	}
}

func HomeData(ctx context.Context) *HomeStruct {
	data, found := c.Get("home")
	if found {
		return data.(*HomeStruct)
	}

	client := &http.Client{}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		"https://content.services.pbskids.org/v2/kidspbsorg/home/",
		nil,
	)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", userAgent)

	select {
	case <-ctx.Done():
		log.Println("Getting home data cancelled")
		return &HomeStruct{}
	default:
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}

		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}

		data := HomeStruct{}
		err = json.Unmarshal(body, &data)
		if err != nil {
			log.Println(err)
		}

		c.SetDefault("home", &data)
		return &data
	}
}

func Foreground(background string) template.CSS {
	if colorblind.ScoreHex(background, "#FFF") < 4.5 {
		return " color: #000;"
	}

	return ""
}

func FormatAges(ages string) string {
	return strings.Replace(ages, "-", " to ", 1)
}

func ProxyImage(url string, options string) string {
	if *proxyEnabled && *proxyKey != "" {
		mac := hmac.New(sha256.New, []byte(*proxyKey))
		mac.Write([]byte(url + "#" + imageproxy.ParseOptions(options).String()))
		result := mac.Sum(nil)
		shortenedURL := strings.Replace(url, "https://image.pbs.org", "", 1)
		return "/proxy/" + options + ",s" + base64.URLEncoding.EncodeToString(result) + shortenedURL
	} else if *proxyEnabled && *proxyKey == "" {
		return strings.Replace(url, "https://image.pbs.org/", "/proxy/"+options+"/", 1)
	}
	return url
}
