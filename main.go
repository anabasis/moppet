// moppet, an alternative frontend for PBS Kids
// Copyright (C) 2022 anabasis
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"crypto/tls"
	"embed"
	"encoding/gob"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/CAFxX/httpcompression"
	"github.com/die-net/lrucache"
	"github.com/die-net/lrucache/twotier"
	"github.com/gregjones/httpcache/diskcache"
	"github.com/patrickmn/go-cache"
	"willnorris.com/go/imageproxy"
)

//go:embed static/* tmpl/*
var content embed.FS

const userAgent = "Mozilla/5.0 (Windows NT 10.0; rv:127.0) Gecko/20100101 Firefox/127.0"

var (
	imageproxyDiskCache, imageproxyMemCache, proxyKey *string
	cacheFile                                         *string
	addr, secureAddr                                  *string

	proxyEnabled, TLSEnabled *bool

	imgCache imageproxy.Cache
	c        *cache.Cache

	proxy *imageproxy.Proxy

	inUS bool
)

func main() {
	// initialize flags
	addr = flag.String("addr", ":80", "default network address (HTTP)")
	secureAddr = flag.String("secure-addr", ":443", "network address for HTTPS")
	TLSEnabled = flag.Bool("tls", false, "enable TLS")
	compression := flag.Bool("compression", true, "enable/disable compression")
	proxyEnabled = flag.Bool("imageproxy", true, "enable/disable image proxying")
	imageproxyDiskCache = flag.String("imageproxy-diskcache-path", "/tmp/moppet-image-cache", "path for cached files (empty to disable)")
	imageproxyMemCache = flag.String(
		"imageproxy-memorycache",
		"200:200",
		"size of imageproxy memory-cache (in MB) followed by colon and time (in hours) (empty to disable)",
	)
	certFile := flag.String("cert", "cert.pem", "certificate PEM file")
	keyFile := flag.String("key", "key.pem", "key PEM file")
	proxyKey = flag.String("proxy-key", "", "secret key for signing proxied images")

	// TODO: fix caching of data
	// cacheFile = flag.String("cache-file", "", "file to save cached data")
	tmpVar := ""
	cacheFile = &tmpVar

	flag.Parse()

	// initialize caching
	InitCache()

	mux := http.NewServeMux()
	mux.HandleFunc("/", Home)
	mux.HandleFunc("/shows/", Shows)
	mux.HandleFunc("/watch/", Watch)
	mux.HandleFunc("/watch/livestream", Live)
	mux.HandleFunc("/assets/", Static)
	mux.HandleFunc("/lib/", Static)
	mux.HandleFunc("/about", Static)
	mux.HandleFunc("/robots.txt", Static)

	if *proxyEnabled {
		mux.HandleFunc("/proxy/", Proxy)

		proxy = imageproxy.NewProxy(nil, imgCache)
		proxy.DefaultBaseURL = &url.URL{Scheme: "https", Host: "image.pbs.org", Path: "/"}
		proxy.UserAgent = userAgent
		if *proxyKey != "" {
			proxy.SignatureKeys = [][]byte{[]byte(*proxyKey)}
		} else {
			proxy.AllowHosts = []string{"image.pbs.org"}
		}
	}

	srv := &http.Server{
		Addr:              *addr,
		Handler:           SecureSite(http.TimeoutHandler(mux, 10*time.Second, "Request timed out!\n")),
		ReadTimeout:       5 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       1 * time.Second,
		ReadHeaderTimeout: 2 * time.Second,
		MaxHeaderBytes:    1 << 20,
		TLSConfig: &tls.Config{
			MinVersion: tls.VersionTLS13,
		},
	}

	if *compression {
		compress, err := httpcompression.DefaultAdapter()
		if err != nil {
			log.Fatalln(err)
		}
		srv.Handler = compress(srv.Handler)
	}

	go func() {
		inUS = IPCheckUS()
		fmt.Printf("server located in US: %v\n", inUS)

		if *TLSEnabled {
			srv.Addr = *secureAddr
			go http.ListenAndServe(*addr, http.HandlerFunc(Redirect))
			if err := srv.ListenAndServeTLS(*certFile, *keyFile); err != nil && err != http.ErrServerClosed {
				log.Fatalf("listen: %s\n", err)
			}
		} else {
			if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				log.Fatalf("listen: %s\n", err)
			}
		}
	}()

	if *TLSEnabled {
		fmt.Println("HTTPS server started at", *secureAddr, "and HTTP server started at", *addr)
	} else {
		fmt.Println("server started at", *addr)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-done
	fmt.Println("server stopped")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		if *cacheFile != "" {
			cacheData := c.Items()

			file, err := os.Create(*cacheFile)
			if err != nil {
				log.Fatalln(err)
			}

			defer file.Close()

			encoder := gob.NewEncoder(file)

			err = encoder.Encode(cacheData)
			if err != nil {
				log.Fatalln(err)
			}
		}

		c.Flush()
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("server shutdown failed:%+v", err)
	}
	fmt.Println("server exited properly")
}

func IPCheckUS() bool {
	client := &http.Client{}

	req, err := http.NewRequest(http.MethodGet, "https://localization.services.pbs.org/localize/auto/", nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", userAgent)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	if resp.StatusCode != 200 {
		return false
	}

	return true
}

func InitCache() error {
	_, err := os.Stat(*cacheFile)

	if *cacheFile == "" || os.IsNotExist(err) {
		c = cache.New(5*time.Minute, 10*time.Minute)
	} else {
		var cacheData map[string]cache.Item

		file, err := os.Open(*cacheFile)
		if err != nil {
			log.Fatalln(err)
		}
		defer file.Close()

		gob.Register(HomeStruct{})
		gob.Register(ProgramStruct{})

		decoder := gob.NewDecoder(file)
		err = decoder.Decode(&cacheData)
		if err != nil {
			log.Fatalln(err)
		}

		c = cache.NewFrom(5*time.Minute, 10*time.Minute, cacheData)
	}

	invalidPath := false

	dir, err := os.Stat(*imageproxyDiskCache)
	if err != nil || !dir.IsDir() {
		if os.IsNotExist(err) && *imageproxyDiskCache != "" {
			err := os.Mkdir(*imageproxyDiskCache, 0o755)
			if err != nil {
				log.Fatalln(err)
			}
		} else {
			invalidPath = true
		}
	}

	memory := strings.Fields(*imageproxyMemCache)
	if len(memory) != 1 && *imageproxyMemCache != "" {
		return errors.New("one field for cache-memory was not provided")
	}

	var memoryConfig [2]int64
	if *imageproxyMemCache != "" {
		fields := strings.Split(memory[0], ":")
		if len(fields) != 2 {
			return errors.New("cache-memory does not follow correct format")
		}
		size, err := strconv.ParseInt(fields[0], 10, 64)
		if err != nil {
			return err
		}
		age, err := strconv.ParseInt(fields[1], 10, 64)
		if err != nil {
			return err
		}
		memoryConfig = [2]int64{size * 1e6, age * 3600}
	}

	switch {
	case *imageproxyMemCache == "" && invalidPath:
		fmt.Println("no cache used")
		imgCache = imageproxy.NopCache

	case *imageproxyMemCache == "" && !invalidPath:
		fmt.Printf("using only disk cache at %s\n", *imageproxyDiskCache)
		imgCache = diskcache.New(*imageproxyDiskCache)

	case *imageproxyMemCache != "" && invalidPath:
		fmt.Println("using only in-memory cache")
		imgCache = lrucache.New(memoryConfig[0], memoryConfig[1])

	case *imageproxyMemCache != "" && !invalidPath:
		fmt.Printf("using in-memory cache and disk cache at %s\n", *imageproxyDiskCache)
		imgCache = twotier.New(
			lrucache.New(memoryConfig[0], memoryConfig[1]),
			diskcache.New(*imageproxyDiskCache),
		)
	}

	return nil
}

func SecureSite(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// NOTE: COEP and CORP headers have not been set to allow for use without JS
		w.Header().Add("X-Frame-Options", "DENY")
		w.Header().Add("X-Content-Type-Options", "nosniff")
		w.Header().Add("Cross-Origin-Opener-Policy", "same-origin")
		w.Header().Add("Referrer-Policy", "no-referrer")

		imgSrc := "img-src 'self'"
		if !*proxyEnabled {
			// enable acess to PBS image if proxy disabled
			imgSrc += " https://image.pbs.org"
		}

		mediaSrc := "media-src 'self' data: blob: https://livestream.pbskids.org https://kids.pbs-video.pbs.org"
		connectSrc := "connect-src 'self' https://livestream.pbskids.org https://kids.pbs-video.pbs.org"
		if !inUS {
			// enable access to PBS redirect service if not in US
			mediaSrc += " https://urs.pbs.org"
			connectSrc += " https://urs.pbs.org"
		}

		csp := fmt.Sprintf(
			"default-src 'none'; base-uri 'none'; frame-ancestors 'none'; font-src 'self' data:; %s; %s; %s; style-src 'self' 'unsafe-inline'; object-src 'none'; script-src 'self' blob:; manifest-src 'self'; worker-src 'self' blob:",
			imgSrc,
			mediaSrc,
			connectSrc,
		)
		w.Header().Add("Content-Security-Policy", csp)

		if *TLSEnabled {
			// TODO: preload list options
			w.Header().Add("Strict-Transport-Security", "max-age=31536000")
		}

		h.ServeHTTP(w, r)
	})
}
