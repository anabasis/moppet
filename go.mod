module moppet

go 1.21

require (
	github.com/CAFxX/httpcompression v0.0.9
	github.com/afonsolopez/colorblind v0.0.0-20210213182922-e55682ffa416
	github.com/die-net/lrucache v0.0.0-20220628165024-20a71bc65bf1
	github.com/gregjones/httpcache v0.0.0-20190611155906-901d90724c79
	github.com/patrickmn/go-cache v2.1.0+incompatible
	willnorris.com/go/imageproxy v0.11.2
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/fcjr/aia-transport-go v1.2.2 // indirect
	github.com/google/btree v1.1.2 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/muesli/smartcrop v0.3.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/peterbourgon/diskv v0.0.0-20171120014656-2973218375c3 // indirect
	github.com/prometheus/client_golang v1.19.1 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.54.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	github.com/rwcarlsen/goexif v0.0.0-20190401172101-9e8deecbddbd // indirect
	golang.org/x/image v0.17.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
	willnorris.com/go/gifresize v1.0.0 // indirect
)
