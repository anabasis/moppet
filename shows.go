// moppet, an alternative frontend for PBS Kids
// Copyright (C) 2022 anabasis
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const pageLength = 25

type ProgramStruct struct {
	Info       info       `json:"object"`
	Collection collection `json:"collections"`
}

type ProgramPaginatedStruct struct {
	Info       info
	Collection collection
	Page       int
	Length     int
}

type format struct {
	Content []video `json:"content"`
	Title   string  `json:"title"`
}

type video struct {
	Title            string    `json:"title"`
	ShortDescription string    `json:"short_description"`
	Description      string    `json:"description"`
	ID               string    `json:"id"`
	AirDate          time.Time `json:"air_date"`
	UploadDate       time.Time `json:"encore_date"`
	ExpireDate       time.Time `json:"expire_date"`
	MP4              string    `json:"mp4"`
	HLS              string    `json:"URI"`
	Duration         int       `json:"duration"`
	Type             string    `json:"video_type"`
	TypeSlug         string
	ClosedCaptions   closedCaptions `json:"closedCaptions"`
	Images           struct {
		Main string `json:"mezzanine"`
	} `json:"images"`
	Program struct {
		Slug  string `json:"slug"`
		Title string `json:"title"`
	} `json:"program"`
}

type info struct {
	Title       string `json:"title"`
	Ages        string `json:"ages"`
	Description string `json:"description"`
	Slug        string `json:"slug"`
	Images      struct {
		Logo       string `json:"logo"`
		Background string `json:"background"`
	} `json:"images"`
}

type collection struct {
	PromotedContent format `json:"promoted_content"`
	Episodes        format `json:"episodes"`
	Clips           format `json:"clips"`
}

type closedCaptions []struct {
	URL    string `json:"URI"`
	Format string `json:"format"`
}

type formatType int

const (
	PromotedContent formatType = iota
	Episodes
	Clips
)

func (data ProgramStruct) toPaginated(page int) ProgramPaginatedStruct {
	contentLength := len(data.Collection.PromotedContent.Content) + len(data.Collection.Episodes.Content) + len(data.Collection.Clips.Content)
	pageNum := int(math.Ceil(float64(contentLength) / float64(pageLength)))

	if page > pageNum {
		return ProgramPaginatedStruct{
			Info:       data.Info,
			Collection: collection{},
			Page:       page,
			Length:     pageNum,
		}
	}

	paginatedCollection := collection{}
	paginatedCollection.PromotedContent.Title = data.Collection.PromotedContent.Title
	paginatedCollection.Episodes.Title = data.Collection.Episodes.Title
	paginatedCollection.Clips.Title = data.Collection.Clips.Title

	var currentFormat formatType
	switch {
	case (page-1)*pageLength < len(data.Collection.PromotedContent.Content):
		currentFormat = PromotedContent
	case (page-1)*pageLength < len(data.Collection.PromotedContent.Content)+len(data.Collection.Episodes.Content):
		currentFormat = Episodes
	default:
		currentFormat = Clips
	}

	addedItems := 0
	for addedItems < pageLength {
		switch currentFormat {
		case PromotedContent:
			startIndex := (page-1)*pageLength + addedItems
			endIndex := min(startIndex+pageLength, len(data.Collection.PromotedContent.Content))
			paginatedCollection.PromotedContent.Content = data.Collection.PromotedContent.Content[startIndex:endIndex]

			addedItems += len(paginatedCollection.PromotedContent.Content)

			currentFormat = Episodes
		case Episodes:
			startIndex := (page-1)*pageLength + addedItems - len(data.Collection.PromotedContent.Content)
			endIndex := min(startIndex+pageLength, len(data.Collection.Episodes.Content))
			paginatedCollection.Episodes.Content = data.Collection.Episodes.Content[startIndex:endIndex]

			addedItems += len(paginatedCollection.Episodes.Content)

			currentFormat = Clips
		case Clips:
			startIndex := (page-1)*pageLength + addedItems - (len(data.Collection.PromotedContent.Content) + len(data.Collection.Episodes.Content))
			endIndex := min(startIndex+pageLength, len(data.Collection.Clips.Content))
			paginatedCollection.Clips.Content = data.Collection.Clips.Content[startIndex:endIndex]

			// exit
			addedItems = pageLength
		}
	}

	return ProgramPaginatedStruct{
		Info:       data.Info,
		Collection: paginatedCollection,
		Page:       page,
		Length:     pageNum,
	}
}

func Shows(w http.ResponseWriter, r *http.Request) {
	split := strings.Split(r.URL.Path, "/")

	// "/shows/" -> "/"
	if len(split) == 3 && split[len(split)-1] == "" {
		http.Redirect(w, r, "/", http.StatusMovedPermanently)

		return
	}

	// "/shows/arthur/" -> "/shows/arthur/1"
	if len(split) == 4 && split[len(split)-1] == "" {
		http.Redirect(w, r, r.URL.Path+"1", http.StatusMovedPermanently)

		return
	}

	// "/shows/arthur" -> "/shows/arthur/1"
	if len(split) == 3 {
		http.Redirect(w, r, r.URL.Path+"/1", http.StatusMovedPermanently)

		return
	}

	// "/shows/arthur/1/" -> "/shows/arthur/1"
	if len(split) == 5 && split[len(split)-1] == "" {
		http.Redirect(w, r, strings.Join(split[:4], "/"), http.StatusMovedPermanently)

		return
	}

	// "/shows/arthur/1/arthur" -> error
	if len(split) != 4 {
		Error(w, r)

		return
	}

	// "/shows/arthur/arthur" -> error
	page, err := strconv.Atoi(split[len(split)-1])
	if err != nil || page <= 0 {
		Error(w, r)

		return
	}

	data := ShowsData(split[2], r.Context())
	if data.Info.Title == "" {
		Error(w, r)

		return
	}

	t, err := template.New("shows.gohtml").Funcs(template.FuncMap{
		"IsNew":          IsNew,
		"FormatDuration": FormatDuration,
		"FormatAges":     FormatAges,
		"ProxyImage":     ProxyImage,
		"PageMenu":       PageMenu,
	}).ParseFS(content, "tmpl/shows.gohtml")
	if err != nil {
		log.Println(err)
	}

	w.Header().Set("Cache-Control", "max-age=86400")
	w.Header().Set("Content-Type", "text/html")

	// TODO: maybe do data.toPaginated(1)
	err = t.Execute(w, data.toPaginated(page))
	if err != nil {
		log.Println(err)
	}
	// TODO: add page number to watch page
}

func ShowsData(show string, ctx context.Context) *ProgramStruct {
	data, found := c.Get("show/" + show)
	if found {
		return data.(*ProgramStruct)
	}

	client := &http.Client{}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		"https://content.services.pbskids.org/v2/kidspbsorg/programs/"+show,
		nil,
	)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", userAgent)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	if resp.StatusCode != 200 {
		data := ProgramStruct{}
		c.SetDefault("show/"+show, &data)

		return &data
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	select {
	case <-ctx.Done():
		log.Println("Getting show data for", show, "cancelled")
		return &ProgramStruct{}
	default:
		data := ProgramStruct{}

		err = json.Unmarshal(body, &data)
		if err != nil {
			log.Println(err)
		}

		c.SetDefault("show/"+show, &data)

		return &data
	}
}

func IsNew(date time.Time) bool {
	return date.After(time.Now().AddDate(0, 0, -7))
}

func FormatDuration(duration int) string {
	if duration >= 3600 {
		return fmt.Sprintf("%02d:%02d:%02d", duration/3600, duration/60%60, duration%60)
	}

	return fmt.Sprintf("%02d:%02d", duration/60, duration%60)
}

func PageMenu(length int, page int, show string) template.HTML {
	builder := strings.Builder{}

	if page-1 > 0 {
		builder.WriteString(fmt.Sprintf("<a href=\"/shows/%s/%v\">&lt;</a>\n", show, page-1))
	} else {
		builder.WriteString("<a class=\"disabled\">&lt;</a>\n")
	}

	for i := 1; i <= length; i++ {
		if i == page {
			builder.WriteString(fmt.Sprintf("<a href=\"/shows/%s/%v\"><b>%v</b></a>\n", show, i, i))
		} else {
			builder.WriteString(fmt.Sprintf("<a href=\"/shows/%s/%v\">%v</a>\n", show, i, i))
		}
	}

	if page+1 <= length {
		builder.WriteString(fmt.Sprintf("<a href=\"/shows/%s/%v\">&gt;</a>\n", show, page+1))
	} else {
		builder.WriteString("<a class=\"disabled\">&gt;</a>\n")
	}

	return template.HTML(builder.String())
}
