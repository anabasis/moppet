let content = `<div class="search"><label>Search by title <input id="search" type="search" placeholder="Video title"></label><br>`;
if (document.querySelectorAll(".heading").length > 1) {
	content += `<label><input id="format" type="checkbox"> Filter by format</label><div id="filterformat" style="display:none;">`
	if (document.querySelector("#episodes")) {
		content += `<label><input value="episodes" name="format" type="checkbox" checked> Episodes</label>`;
	}
	if (document.querySelector("#clips")) {
		content += `<label><input value="clips" name="format" type="checkbox" checked> Clips</label>`;
	}
	if (document.querySelector("#promoted_content")) {
		content += `<label><input value="promoted_content" name="format" type="checkbox" checked> Promoted Content</label>`;
	}
}
content += "</div></div>";

document.querySelector("header").insertAdjacentHTML("afterend", content);
const cards = document.querySelectorAll(".card");
const articles = document.querySelectorAll("article");

const shows = [];
for (let show of articles) {
	shows.push({"title": show.title, "id": show.id, "description": show.querySelector("p.ds").innerText});
}

const search = document.querySelector("#search");
const format = document.querySelector("#format");

const find = () => {
	for (let show of articles) {
		show.classList.remove("nomatch")
	}

	let query = search.value.toLowerCase();
	let nomatch = shows.filter(show => !show.title.toLowerCase().includes(query) && !show.description.toLowerCase().includes(query));
	if (format) {
		if (format.checked !== true) {
			document.querySelector("#filterformat").style.display = "none";
			for (let input of document.querySelectorAll(`input[name="format"]`)) {
				document.getElementById(input.value).parentElement.parentElement.style.display = "";
			}
		} else {
			document.getElementById("filterformat").style = "";
			for (let input of document.querySelectorAll(`input[name="format"]`)) {
				document.getElementById(input.value).parentElement.parentElement.style.display = "none";
			}
			for (let input of document.querySelectorAll(`input[name="format"]:checked`)) {
				document.getElementById(input.value).parentElement.parentElement.style.display = "";
			}
		}
	}

	for (let show of nomatch) {
		document.getElementById(show.id).classList.add("nomatch");
	}
}

find();
search.addEventListener("keyup", find);
if (format) {
	format.addEventListener("change", find);
}
for (let x of document.querySelectorAll(`input[name="format"]`)) {
	x.addEventListener("change", find)
}
