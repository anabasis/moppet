document.querySelector(".search").style = "";
const cards = document.querySelectorAll(".card");
const articles = document.querySelectorAll("article");

const shows = [];
for (let show of articles) {
	let ages = show.getAttribute("data-age").split("-");
	shows.push({ "id": show.id, "title": show.title, "start": ages[0], "end": ages[1] });
}

const search = document.querySelector("#search");
const start = document.querySelector("#start");
const end = document.querySelector("#end");
const age = document.querySelector("#age");

const find = () => {
	for (let show of articles) {
		show.classList.remove("nomatch");
	}

	let query = search.value.toLowerCase();
	let startage = parseInt(start.value);
	let endage = parseInt(end.value);

	if (startage > endage) {
		start.value = end.value;
		startage = endage;
	}

	if (age.checked !== true) {
		document.getElementById("filterage").style.display = "none";
	} else {
		document.getElementById("filterage").style.display = "";
	}

	let nomatch = shows.filter((show) => {
		let nomatchquery = !show.title.toLowerCase().includes(query) && !show.id.toLowerCase().includes(query);
		let nomatchage = startage < show.start || endage > show.end;
		if (!age.checked) {
			nomatchage = false;
		}
		return (nomatchquery || nomatchage);
	});

	for (let show of nomatch) {
		document.getElementById(show.id).classList.add("nomatch");
	}
}

find();
search.addEventListener("keyup", find);
start.addEventListener("change", find);
end.addEventListener("change", find);
age.addEventListener("change", find);
