const player = videojs("player", {
	controls: true,
	fill: true,
	liveui: true,
	playbackRates: [0.75, 1, 1.25, 1.5, 1.75, 2],
	plugins: {
		hotkeys: {
			alwaysCaptureHotkeys: true,
			seekStep: 10,
			captureDocumentHotkeys: true,
			documentHotkeysFocusElementFilter: (e) => { e.tagName.toLowerCase() === "body" },
			customKeys: {
				increaseSpeed: {
					key: (e) => e.key === ">",
					handler: (e) => {
						if (e.playbackRate() < 4) {
							e.playbackRate(e.playbackRate() + 0.25);
						}
					},
				},
				decreaseSpeed: {
					key: (e) => e.key === "<",
					handler: (e) => {
						if (e.playbackRate() > 0.25) {
							e.playbackRate(e.playbackRate() - 0.25);
						}
					},
				},
				toggleCaptions: {
					key: (e) => e.key === "c",
					handler: (e) => {
						const textTracks = e.textTracks();
						for (let i = 0; i < textTracks.length; i++) {
							let track = textTracks[i];
							if (track.kind !== "subtitles") {
								continue;
							}
							if (track.mode === "showing") {
								track.mode = "disabled";
								break;
							}
							if (track.mode === "disabled" && track.language === e.language()) {
								track.mode = "showing";
								break;
							}
						}
					},
				},
			},
		},
	},
});

player.hlsQualitySelector();

document.querySelector(".vjs-loading-spinner").innerHTML = `<div></div><div></div><div></div><div></div><span class="vjs-control-text">Video Player is loading.</span>`;
