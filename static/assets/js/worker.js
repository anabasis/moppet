// adapted from https://github.com/mdn/pwa-examples (available under the CC0-1.0 license)

const cacheName = "moppetPWA";
const cacheFiles = [
	"/",

	"/assets/css/style.css",
	"/assets/css/videojs-style.css",
	"/assets/css/fonts/league-spartan.woff2",
	"/assets/css/fonts/lexend-deca.ttf",
	"/assets/css/fonts/outfit.ttf",

	"/assets/img/about.svg",
	"/assets/img/back.svg",
	"/assets/img/code.svg",
	"/assets/img/home.svg",
	"/assets/img/livestream.svg",
	"/assets/img/logo.svg",
	"/assets/img/logos/logo48.png",
	"/assets/img/logos/logo72.png",
	"/assets/img/logos/logo96.png",
	"/assets/img/logos/logo144.png",
	"/assets/img/logos/logo168.png",
	"/assets/img/logos/logo192.png",
	"/assets/img/logos/logo512.png",

	"/assets/js/videojs-settings.js",
	"/assets/js/lib/video-js.min.css",
	"/assets/js/lib/video.min.js",
	"/assets/js/lib/videojs-contrib-quality-levels.min.js",
	"/assets/js/lib/videojs-hls-quality-selector.min.js",
	"/assets/js/lib/videojs.hotkeys.min.js"
];

self.addEventListener("install", (e) => {
	console.log("[Service Worker] Install");
	e.waitUntil((async () => {
		const cache = await caches.open(cacheName);
		console.log("[Service Worker] Caching all: app shell and content");
		await cache.addAll(cacheFiles);
	})());
});

self.addEventListener("fetch", (e) => {
	e.respondWith((async () => {
		const r = await caches.match(e.request);
		console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
		if (r) { return r; }
		const response = await fetch(e.request);
		const cache = await caches.open(cacheName);
		console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
		cache.put(e.request, response.clone());
		return response;
	})());
});

self.addEventListener("activate", (e) => {
	e.waitUntil(caches.keys().then((keyList) => {
		return Promise.all(keyList.map((key) => {
			if (key === cacheName) { return; }
			return caches.delete(key);
		}))
	}));
});
